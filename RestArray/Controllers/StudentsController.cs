﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestArray.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RestArray.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        public StudentsController()
        {

        }

        public List<Student> Students = new List<Student>
            { new Student {Id=1, Name="Asan", Gender="Male", Department="ECE", DateOfJoining=DateTime.Now},

              new Student {Id=2, Name="Hyder", Gender="Male", Department="CS", DateOfJoining=DateTime.Now},

              new Student {Id=3, Name="Kadher", Gender="Male", Department="CS", DateOfJoining=DateTime.Now}
            };
        

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(Students);
        }

        [HttpGet("{id}")]
        public Student Get(int id)
        {
            var studentSpecific = Students.Single(x => x.Id == id);
            return studentSpecific;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Student studentNew)
        {
           Students.Add(studentNew);  
           return Ok("Posted the record successfully");
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, Student records)
        {
            var studentUpdate = Students.Single(x => x.Id == id);

            studentUpdate.Id = records.Id;
            studentUpdate.Name = records.Name;
            studentUpdate.Department = records.Department;
            studentUpdate.Gender = records.Gender;
            studentUpdate.DateOfJoining = DateTime.Now;

            return Ok("Updated the record successfully");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var studentdelete = Students.Single(x => x.Id == id);
            Students.Remove(studentdelete);

            return Ok("Deleted the record successfully");
        }
    }
}
